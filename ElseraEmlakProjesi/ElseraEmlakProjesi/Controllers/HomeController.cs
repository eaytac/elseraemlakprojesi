﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using ElseraEmlakProjesi.Models;

namespace ElseraEmlakProjesi.Controllers
{
    public class HomeController : Controller
    {
       
        public ActionResult Index()
        {
            ViewBag.Slider = db.Slider.ToList();
            return View();
        }

        public ActionResult Konutlar()
        {
            List<ilanViewModel> ilanlar = new List<ilanViewModel>();
            var ilan = db.ilan.Where(x => x.Kategori.KategoriAdi == "Konut").ToList();
            foreach (var item in ilan)
            {
                ilanlar.Add(new ilanViewModel()
                {
                    ilan = item,
                    Fotograflar = db.Foto.Where(x => x.ilanID == item.ID).ToList()
                });
            }
            return View(ilanlar);
        }

        EmlakVeriTabaniEntities1 db = new EmlakVeriTabaniEntities1();
        public ActionResult Konut(int id)
        {
            ilanViewModel ilan = new ilanViewModel();
            ilan.ilan = db.ilan.Single(x => x.ID == id);
            ilan.Fotograflar = db.Foto.Where(x => x.ilanID == id).ToList();
            return View(ilan);

        }

        public ActionResult IsYerleri()
        {
            List<ilanViewModel> ilanlar = new List<ilanViewModel>();
            var ilan = db.ilan.Where(x => x.Kategori.KategoriAdi == "İş Yeri").ToList();
            foreach (var item in ilan)
            {
                ilanlar.Add(new ilanViewModel()
                {
                    ilan = item,
                    Fotograflar = db.Foto.Where(x => x.ilanID == item.ID).ToList()
                });
            }
            return View(ilanlar);
        }

        public ActionResult IsYeri(int id)
        {
            ilanViewModel ilan = new ilanViewModel();
            ilan.ilan = db.ilan.Single(x => x.ID == id);
            ilan.Fotograflar = db.Foto.Where(x => x.ilanID == id).ToList();
            return View(ilan);
        }


        public ActionResult Arsalar()
        {
            List<ilanViewModel> ilanlar = new List<ilanViewModel>();
            var ilan = db.ilan.Where(x => x.Kategori.KategoriAdi == "Arsa").ToList();
            foreach (var item in ilan)
            {
                ilanlar.Add(new ilanViewModel()
                {
                    ilan = item,
                    Fotograflar = db.Foto.Where(x => x.ilanID == item.ID).ToList()
                });
            }
            return View(ilanlar);
        }

        public ActionResult Arsa(int id)
        {
            ilanViewModel ilan = new ilanViewModel();
            ilan.ilan = db.ilan.Single(x => x.ID == id);
            ilan.Fotograflar = db.Foto.Where(x => x.ilanID == id).ToList();
            return View(ilan);
        }



        public ActionResult Hakkimizda()
        {
            ViewBag.Message = "Hakkımızda";

            return View();
        }

        public ActionResult KurumsalMateryal()
        {
            ViewBag.Message = "Kurumsal Materyallerimiz";

            return View();
        }
        public ActionResult insankaynaklari()
        {
            ViewBag.Message = "İnsan Kaynakları";

            return View();
        }


       /* public ActionResult ChangeCulture(string lang, string returnUrl)
        {
            Session["Culture"] = new CultureInfo(lang);
            return Redirect(returnUrl);
        }*/



        public ActionResult About()
        {
            ViewBag.Message = "Your application description page.";

            return View();
        }

        public ActionResult Contact()
        {
            ViewBag.Message = "Bize ulasmak için; ";

            return View();
        }

        public ActionResult GoogleSearch()
        {

            return View();
        }
    }
}