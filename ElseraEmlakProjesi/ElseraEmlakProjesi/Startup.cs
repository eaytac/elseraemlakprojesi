﻿using Microsoft.Owin;
using Owin;

[assembly: OwinStartupAttribute(typeof(ElseraEmlakProjesi.Startup))]
namespace ElseraEmlakProjesi
{
    public partial class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            ConfigureAuth(app);
        }
    }
}
