﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using ElseraEmlakProjesi.Models;

namespace ElseraEmlakProjesi.Areas.Admin.Controllers
{
    public class DisOzelliklerController : Controller
    {
        private EmlakVeriTabaniEntities1 db = new EmlakVeriTabaniEntities1();

        // GET: Admin/DisOzellikler
        public ActionResult Index()
        {
            return View(db.DisOzellikler.ToList());
        }

        // GET: Admin/DisOzellikler/Details/5
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            DisOzellikler disOzellikler = db.DisOzellikler.Find(id);
            if (disOzellikler == null)
            {
                return HttpNotFound();
            }
            return View(disOzellikler);
        }

        // GET: Admin/DisOzellikler/Create
        public ActionResult Create()
        {
            return View();
        }

        // POST: Admin/DisOzellikler/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "ID,OzellikAdi")] DisOzellikler disOzellikler)
        {
            if (ModelState.IsValid)
            {
                db.DisOzellikler.Add(disOzellikler);
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            return View(disOzellikler);
        }

        // GET: Admin/DisOzellikler/Edit/5
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            DisOzellikler disOzellikler = db.DisOzellikler.Find(id);
            if (disOzellikler == null)
            {
                return HttpNotFound();
            }
            return View(disOzellikler);
        }

        // POST: Admin/DisOzellikler/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "ID,OzellikAdi")] DisOzellikler disOzellikler)
        {
            if (ModelState.IsValid)
            {
                db.Entry(disOzellikler).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            return View(disOzellikler);
        }

        // GET: Admin/DisOzellikler/Delete/5
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            DisOzellikler disOzellikler = db.DisOzellikler.Find(id);
            if (disOzellikler == null)
            {
                return HttpNotFound();
            }
            return View(disOzellikler);
        }

        // POST: Admin/DisOzellikler/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            DisOzellikler disOzellikler = db.DisOzellikler.Find(id);
            db.DisOzellikler.Remove(disOzellikler);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
