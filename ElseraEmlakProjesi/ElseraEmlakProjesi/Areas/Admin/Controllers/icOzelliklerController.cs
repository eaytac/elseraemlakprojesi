﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using ElseraEmlakProjesi.Models;

namespace ElseraEmlakProjesi.Areas.Admin.Controllers
{
    public class icOzelliklerController : Controller
    {
        private EmlakVeriTabaniEntities1 db = new EmlakVeriTabaniEntities1();

        // GET: Admin/icOzellikler
        public ActionResult Index()
        {
            return View(db.icOzellikler.ToList());
        }

        // GET: Admin/icOzellikler/Details/5
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            icOzellikler icOzellikler = db.icOzellikler.Find(id);
            if (icOzellikler == null)
            {
                return HttpNotFound();
            }
            return View(icOzellikler);
        }

        // GET: Admin/icOzellikler/Create
        public ActionResult Create()
        {
            return View();
        }

        // POST: Admin/icOzellikler/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "ID,OzellikAdi")] icOzellikler icOzellikler)
        {
            if (ModelState.IsValid)
            {
                db.icOzellikler.Add(icOzellikler);
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            return View(icOzellikler);
        }

        // GET: Admin/icOzellikler/Edit/5
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            icOzellikler icOzellikler = db.icOzellikler.Find(id);
            if (icOzellikler == null)
            {
                return HttpNotFound();
            }
            return View(icOzellikler);
        }

        // POST: Admin/icOzellikler/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "ID,OzellikAdi")] icOzellikler icOzellikler)
        {
            if (ModelState.IsValid)
            {
                db.Entry(icOzellikler).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            return View(icOzellikler);
        }

        // GET: Admin/icOzellikler/Delete/5
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            icOzellikler icOzellikler = db.icOzellikler.Find(id);
            if (icOzellikler == null)
            {
                return HttpNotFound();
            }
            return View(icOzellikler);
        }

        // POST: Admin/icOzellikler/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            icOzellikler icOzellikler = db.icOzellikler.Find(id);
            db.icOzellikler.Remove(icOzellikler);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
