﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using ElseraEmlakProjesi.Models;

namespace ElseraEmlakProjesi.Areas.Admin.Controllers
{
    public class ilanDetayController : Controller
    {
        private EmlakVeriTabaniEntities1 db = new EmlakVeriTabaniEntities1();

        // GET: Admin/ilanDetay
        public ActionResult Index()
        {
            var ilanDetay = db.ilanDetay.Include(i => i.ilan);
            return View(ilanDetay.ToList());
        }

        // GET: Admin/ilanDetay/Details/5
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            ilanDetay ilanDetay = db.ilanDetay.Find(id);
            if (ilanDetay == null)
            {
                return HttpNotFound();
            }
            return View(ilanDetay);
        }

        // GET: Admin/ilanDetay/Create
        public ActionResult Create()
        {
            ViewBag.ilanID = new SelectList(db.ilan, "ID", "Baslik");
            return View();
        }

        // POST: Admin/ilanDetay/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "ilanID,OdaSayisi,BanyoSayisi,BinaYasi,BinaKatSayisi,BulunduguKatSayisi,isitma,Esyalimi,KrediyeUygun,Aciklama,Konum")] ilanDetay ilanDetay)
        {
            if (ModelState.IsValid)
            {
                db.ilanDetay.Add(ilanDetay);
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            ViewBag.ilanID = new SelectList(db.ilan, "ID", "Baslik", ilanDetay.ilanID);
            return View(ilanDetay);
        }

        // GET: Admin/ilanDetay/Edit/5
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            ilanDetay ilanDetay = db.ilanDetay.Find(id);
            if (ilanDetay == null)
            {
                return HttpNotFound();
            }
            ViewBag.ilanID = new SelectList(db.ilan, "ID", "Baslik", ilanDetay.ilanID);
            return View(ilanDetay);
        }

        // POST: Admin/ilanDetay/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "ilanID,OdaSayisi,BanyoSayisi,BinaYasi,BinaKatSayisi,BulunduguKatSayisi,isitma,Esyalimi,KrediyeUygun,Aciklama,Konum")] ilanDetay ilanDetay)
        {
            if (ModelState.IsValid)
            {
                db.Entry(ilanDetay).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            ViewBag.ilanID = new SelectList(db.ilan, "ID", "Baslik", ilanDetay.ilanID);
            return View(ilanDetay);
        }

        // GET: Admin/ilanDetay/Delete/5
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            ilanDetay ilanDetay = db.ilanDetay.Find(id);
            if (ilanDetay == null)
            {
                return HttpNotFound();
            }
            return View(ilanDetay);
        }

        // POST: Admin/ilanDetay/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            ilanDetay ilanDetay = db.ilanDetay.Find(id);
            db.ilanDetay.Remove(ilanDetay);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
