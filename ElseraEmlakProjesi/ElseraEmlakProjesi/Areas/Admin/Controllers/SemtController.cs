﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using ElseraEmlakProjesi.Models;

namespace ElseraEmlakProjesi.Areas.Admin.Controllers
{
    public class SemtController : Controller
    {
        private EmlakVeriTabaniEntities1 db = new EmlakVeriTabaniEntities1();

        // GET: Admin/Semt
        public ActionResult Index()
        {
            var semt = db.Semt.Include(s => s.ilce);
            return View(semt.ToList());
        }

        // GET: Admin/Semt/Details/5
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Semt semt = db.Semt.Find(id);
            if (semt == null)
            {
                return HttpNotFound();
            }
            return View(semt);
        }

        // GET: Admin/Semt/Create
        public ActionResult Create()
        {
            ViewBag.ilceID = new SelectList(db.ilce, "ID", "ilceAdi");
            return View();
        }

        // POST: Admin/Semt/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "ID,SemtAdi,ilceID")] Semt semt)
        {
            if (ModelState.IsValid)
            {
                db.Semt.Add(semt);
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            ViewBag.ilceID = new SelectList(db.ilce, "ID", "ilceAdi", semt.ilceID);
            return View(semt);
        }

        // GET: Admin/Semt/Edit/5
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Semt semt = db.Semt.Find(id);
            if (semt == null)
            {
                return HttpNotFound();
            }
            ViewBag.ilceID = new SelectList(db.ilce, "ID", "ilceAdi", semt.ilceID);
            return View(semt);
        }

        // POST: Admin/Semt/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "ID,SemtAdi,ilceID")] Semt semt)
        {
            if (ModelState.IsValid)
            {
                db.Entry(semt).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            ViewBag.ilceID = new SelectList(db.ilce, "ID", "ilceAdi", semt.ilceID);
            return View(semt);
        }

        // GET: Admin/Semt/Delete/5
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Semt semt = db.Semt.Find(id);
            if (semt == null)
            {
                return HttpNotFound();
            }
            return View(semt);
        }

        // POST: Admin/Semt/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            Semt semt = db.Semt.Find(id);
            db.Semt.Remove(semt);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
