﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using ElseraEmlakProjesi.Models;

namespace ElseraEmlakProjesi.Areas.Admin.Controllers
{
    public class ilController : Controller
    {
        private EmlakVeriTabaniEntities1 db = new EmlakVeriTabaniEntities1();

        // GET: Admin/il
        public ActionResult Index()
        {
            return View(db.il.ToList());
        }

        // GET: Admin/il/Details/5
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            il il = db.il.Find(id);
            if (il == null)
            {
                return HttpNotFound();
            }
            return View(il);
        }

        // GET: Admin/il/Create
        public ActionResult Create()
        {
            return View();
        }

        // POST: Admin/il/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "ID,ilAdi")] il il)
        {
            if (ModelState.IsValid)
            {
                db.il.Add(il);
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            return View(il);
        }

        // GET: Admin/il/Edit/5
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            il il = db.il.Find(id);
            if (il == null)
            {
                return HttpNotFound();
            }
            return View(il);
        }

        // POST: Admin/il/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "ID,ilAdi")] il il)
        {
            if (ModelState.IsValid)
            {
                db.Entry(il).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            return View(il);
        }

        // GET: Admin/il/Delete/5
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            il il = db.il.Find(id);
            if (il == null)
            {
                return HttpNotFound();
            }
            return View(il);
        }

        // POST: Admin/il/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            il il = db.il.Find(id);
            db.il.Remove(il);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
