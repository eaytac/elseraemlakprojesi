﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using ElseraEmlakProjesi.Models;

namespace ElseraEmlakProjesi.Areas.Admin.Controllers
{
    public class ilceController : Controller
    {
        private EmlakVeriTabaniEntities1 db = new EmlakVeriTabaniEntities1();

        // GET: Admin/ilce
        public ActionResult Index()
        {
            var ilce = db.ilce.Include(i => i.il);
            return View(ilce.ToList());
        }

        // GET: Admin/ilce/Details/5
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            ilce ilce = db.ilce.Find(id);
            if (ilce == null)
            {
                return HttpNotFound();
            }
            return View(ilce);
        }

        // GET: Admin/ilce/Create
        public ActionResult Create()
        {
            ViewBag.ilID = new SelectList(db.il, "ID", "ilAdi");
            return View();
        }

        // POST: Admin/ilce/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "ID,ilceAdi,ilID")] ilce ilce)
        {
            if (ModelState.IsValid)
            {
                db.ilce.Add(ilce);
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            ViewBag.ilID = new SelectList(db.il, "ID", "ilAdi", ilce.ilID);
            return View(ilce);
        }

        // GET: Admin/ilce/Edit/5
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            ilce ilce = db.ilce.Find(id);
            if (ilce == null)
            {
                return HttpNotFound();
            }
            ViewBag.ilID = new SelectList(db.il, "ID", "ilAdi", ilce.ilID);
            return View(ilce);
        }

        // POST: Admin/ilce/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "ID,ilceAdi,ilID")] ilce ilce)
        {
            if (ModelState.IsValid)
            {
                db.Entry(ilce).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            ViewBag.ilID = new SelectList(db.il, "ID", "ilAdi", ilce.ilID);
            return View(ilce);
        }

        // GET: Admin/ilce/Delete/5
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            ilce ilce = db.ilce.Find(id);
            if (ilce == null)
            {
                return HttpNotFound();
            }
            return View(ilce);
        }

        // POST: Admin/ilce/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            ilce ilce = db.ilce.Find(id);
            db.ilce.Remove(ilce);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
