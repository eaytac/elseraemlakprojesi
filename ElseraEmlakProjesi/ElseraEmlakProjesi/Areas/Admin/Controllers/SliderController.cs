﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using ElseraEmlakProjesi.Models;
using System.IO;

namespace ElseraEmlakProjesi.Areas.Admin.Controllers
{
    public class SliderController : Controller
    {
        private EmlakVeriTabaniEntities1 db = new EmlakVeriTabaniEntities1();
        public string UploadImage(HttpPostedFileBase image)
        {
            var imagePath = Path.Combine(Server.MapPath("~/Content/images/upload/"), image.FileName);
            var imageUrl = Path.Combine("~/Content/images/upload/", image.FileName);
            while (System.IO.File.Exists(imagePath))
            {
                Random rnd = new Random();
                string fileName = Path.GetFileNameWithoutExtension(image.FileName) + "-" + rnd.Next(0, 999) + Path.GetExtension(image.FileName);
                imagePath = Path.Combine(Server.MapPath("~/Content/images/upload/"), fileName);
                imageUrl = Path.Combine("~/Content/images/upload/", fileName);
            }
            image.SaveAs(imagePath);
            return imageUrl;
        }



        // GET: Admin/Slider
        public ActionResult Index()
        {
            var slider = db.Foto.Include(f => f.ilan);
            return View(db.Slider.ToList());
        }

        // GET: Admin/Slider/Details/5
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Slider slider = db.Slider.Find(id);
            if (slider == null)
            {
                return HttpNotFound();
            }
            return View(slider);
        }

        // GET: Admin/Slider/Create
        public ActionResult Create()
        {
            ViewBag.ilanID = new SelectList(db.ilan, "ID", "SliderUrl");
            return View();
        }

        // POST: Admin/Slider/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "SliderID,SliderFoto,SliderText,BaslangicTarih,BitisTarih,SliderUrl")] Slider slider, HttpPostedFileBase image)
        {
            if (ModelState.IsValid)
            {
                var imageUrl = UploadImage(image);
                slider.SliderUrl = imageUrl;
                db.Slider.Add(slider);
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            ViewBag.ilanID = new SelectList(db.ilan, "ID", "SliderText", slider.SliderText);
            return View(slider);
        }

        // GET: Admin/Slider/Edit/5
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Slider slider = db.Slider.Find(id);
            if (slider == null)
            {
                return HttpNotFound();
            }
            return View(slider);
        }

        // POST: Admin/Slider/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "SliderID,SliderFoto,SliderText,BaslangicTarih,BitisTarih,SliderUrl")] Slider slider)
        {
            if (ModelState.IsValid)
            {
                db.Entry(slider).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            return View(slider);
        }

        // GET: Admin/Slider/Delete/5
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Slider slider = db.Slider.Find(id);
            if (slider == null)
            {
                return HttpNotFound();
            }
            return View(slider);
        }

        // POST: Admin/Slider/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            Slider slider = db.Slider.Find(id);
            db.Slider.Remove(slider);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
