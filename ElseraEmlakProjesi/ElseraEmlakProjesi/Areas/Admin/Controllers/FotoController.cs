﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using ElseraEmlakProjesi.Models;
using System.IO;

namespace ElseraEmlakProjesi.Areas.Admin.Controllers
{
    public class FotoController : Controller
    {
        private EmlakVeriTabaniEntities1 db = new EmlakVeriTabaniEntities1();
        public string UploadImage(HttpPostedFileBase image)
        {
            var imagePath = Path.Combine(Server.MapPath("~/Content/images/upload/"), image.FileName);
            var imageUrl = Path.Combine("~/Content/images/upload/", image.FileName);
            while (System.IO.File.Exists(imagePath))
            {
                Random rnd = new Random();
                string fileName = Path.GetFileNameWithoutExtension(image.FileName) + "-" + rnd.Next(0, 999) + Path.GetExtension(image.FileName);
                imagePath = Path.Combine(Server.MapPath("~/Content/images/upload/"), fileName);
                imageUrl = Path.Combine("~/Content/images/upload/", fileName);
            }
            image.SaveAs(imagePath);
            return imageUrl;
        }

        // GET: Admin/Foto
        public ActionResult Index()
        {
            var foto = db.Foto.Include(f => f.ilan);
            return View(foto.ToList());
        }

        // GET: Admin/Foto/Details/5
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Foto foto = db.Foto.Find(id);
            if (foto == null)
            {
                return HttpNotFound();
            }
            return View(foto);
        }

        // GET: Admin/Foto/Create
        public ActionResult Create()
        {
            ViewBag.ilanID = new SelectList(db.ilan, "ID", "Baslik");
            return View();
        }

        // POST: Admin/Foto/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "ID,FotoSirala,ilanID")] Foto foto, HttpPostedFileBase image)
        {
            if (ModelState.IsValid)
            {
                var imageUrl = UploadImage(image);
                foto.FotoAdi = imageUrl;
                db.Foto.Add(foto);
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            ViewBag.ilanID = new SelectList(db.ilan, "ID", "Baslik", foto.ilanID);
            return View(foto);
        }

        // GET: Admin/Foto/Edit/5
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Foto foto = db.Foto.Find(id);
            if (foto == null)
            {
                return HttpNotFound();
            }
            ViewBag.ilanID = new SelectList(db.ilan, "ID", "Baslik", foto.ilanID);
            return View(foto);
        }

        // POST: Admin/Foto/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "ID,FotoAdi,FotoSirala,ilanID")] Foto foto)
        {
            if (ModelState.IsValid)
            {
                db.Entry(foto).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            ViewBag.ilanID = new SelectList(db.ilan, "ID", "Baslik", foto.ilanID);
            return View(foto);
        }

        // GET: Admin/Foto/Delete/5
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Foto foto = db.Foto.Find(id);
            if (foto == null)
            {
                return HttpNotFound();
            }
            return View(foto);
        }

        // POST: Admin/Foto/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            Foto foto = db.Foto.Find(id);
            db.Foto.Remove(foto);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
 