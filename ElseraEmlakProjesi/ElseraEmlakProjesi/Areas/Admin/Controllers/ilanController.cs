﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using ElseraEmlakProjesi.Models;

namespace ElseraEmlakProjesi.Areas.Admin.Controllers
{
    public class ilanController : Controller
    {
        private EmlakVeriTabaniEntities1 db = new EmlakVeriTabaniEntities1();

        // GET: Admin/ilan
        public ActionResult Index()
        {
            var ilan = db.ilan.Include(i => i.Cesit).Include(i => i.ilanDetay).Include(i => i.Kategori).Include(i => i.Kullanici).Include(i => i.Semt);
            return View(ilan.ToList());
        }

        // GET: Admin/ilan/Details/5
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            ilan ilan = db.ilan.Find(id);
            if (ilan == null)
            {
                return HttpNotFound();
            }
            return View(ilan);
        }

        // GET: Admin/ilan/Create
        public ActionResult Create()
        {
            ViewBag.CesitID = new SelectList(db.Cesit, "ID", "CesitAdi");
            ViewBag.ID = new SelectList(db.ilanDetay, "ilanID", "OdaSayisi");
            ViewBag.KategoriID = new SelectList(db.Kategori, "ID", "KategoriAdi");
            ViewBag.KullaniciID = new SelectList(db.Kullanici, "ID", "Email");
            ViewBag.SemtID = new SelectList(db.Semt, "ID", "SemtAdi");
            ViewBag.DetayID = new SelectList(db.ilanDetay, "ilanID", "ilanID");
            return View();
        }

        // POST: Admin/ilan/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "ID,Baslik,Fiyat,MetreKare,OlusturmaTarihi,SemtID,KategoriID,CesitID,KullaniciID")] ilan ilan, int DetayID)
        {
            if (ModelState.IsValid)
            {
                ilan.DetayID = DetayID;
                db.ilan.Add(ilan);
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            ViewBag.CesitID = new SelectList(db.Cesit, "ID", "CesitAdi", ilan.CesitID);
            ViewBag.ID = new SelectList(db.ilanDetay, "ilanID", "OdaSayisi", ilan.ID);
            ViewBag.KategoriID = new SelectList(db.Kategori, "ID", "KategoriAdi", ilan.KategoriID);
            ViewBag.KullaniciID = new SelectList(db.Kullanici, "ID", "Email", ilan.KullaniciID);
            ViewBag.SemtID = new SelectList(db.Semt, "ID", "SemtAdi", ilan.SemtID);
            return View(ilan);
        }

        // GET: Admin/ilan/Edit/5
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            ilan ilan = db.ilan.Find(id);
            if (ilan == null)
            {
                return HttpNotFound();
            }
            ViewBag.CesitID = new SelectList(db.Cesit, "ID", "CesitAdi", ilan.CesitID);
            ViewBag.ID = new SelectList(db.ilanDetay, "ilanID", "OdaSayisi", ilan.ID);
            ViewBag.KategoriID = new SelectList(db.Kategori, "ID", "KategoriAdi", ilan.KategoriID);
            ViewBag.KullaniciID = new SelectList(db.Kullanici, "ID", "Email", ilan.KullaniciID);
            ViewBag.SemtID = new SelectList(db.Semt, "ID", "SemtAdi", ilan.SemtID);
            return View(ilan);
        }

        // POST: Admin/ilan/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "ID,Baslik,Fiyat,MetreKare,OlusturmaTarihi,SemtID,KategoriID,CesitID,KullaniciID")] ilan ilan)
        {
            if (ModelState.IsValid)
            {
                db.Entry(ilan).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            ViewBag.CesitID = new SelectList(db.Cesit, "ID", "CesitAdi", ilan.CesitID);
            ViewBag.ID = new SelectList(db.ilanDetay, "ilanID", "OdaSayisi", ilan.ID);
            ViewBag.KategoriID = new SelectList(db.Kategori, "ID", "KategoriAdi", ilan.KategoriID);
            ViewBag.KullaniciID = new SelectList(db.Kullanici, "ID", "Email", ilan.KullaniciID);
            ViewBag.SemtID = new SelectList(db.Semt, "ID", "SemtAdi", ilan.SemtID);
            return View(ilan);
        }

        // GET: Admin/ilan/Delete/5
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            ilan ilan = db.ilan.Find(id);
            if (ilan == null)
            {
                return HttpNotFound();
            }
            return View(ilan);
        }

        // POST: Admin/ilan/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            ilan ilan = db.ilan.Find(id);
            db.ilan.Remove(ilan);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
