﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using ElseraEmlakProjesi.Models;

namespace ElseraEmlakProjesi.Areas.Admin.Controllers
{
    public class CesitController : Controller
    {
        private EmlakVeriTabaniEntities1 db = new EmlakVeriTabaniEntities1();

        // GET: Admin/Cesit
        public ActionResult Index()
        {
            return View(db.Cesit.ToList());
        }

        // GET: Admin/Cesit/Details/5
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Cesit cesit = db.Cesit.Find(id);
            if (cesit == null)
            {
                return HttpNotFound();
            }
            return View(cesit);
        }

        // GET: Admin/Cesit/Create
        public ActionResult Create()
        {
            return View();
        }

        // POST: Admin/Cesit/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "ID,CesitAdi")] Cesit cesit)
        {
            if (ModelState.IsValid)
            {
                db.Cesit.Add(cesit);
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            return View(cesit);
        }

        // GET: Admin/Cesit/Edit/5
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Cesit cesit = db.Cesit.Find(id);
            if (cesit == null)
            {
                return HttpNotFound();
            }
            return View(cesit);
        }

        // POST: Admin/Cesit/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "ID,CesitAdi")] Cesit cesit)
        {
            if (ModelState.IsValid)
            {
                db.Entry(cesit).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            return View(cesit);
        }

        // GET: Admin/Cesit/Delete/5
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Cesit cesit = db.Cesit.Find(id);
            if (cesit == null)
            {
                return HttpNotFound();
            }
            return View(cesit);
        }

        // POST: Admin/Cesit/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            Cesit cesit = db.Cesit.Find(id);
            db.Cesit.Remove(cesit);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
